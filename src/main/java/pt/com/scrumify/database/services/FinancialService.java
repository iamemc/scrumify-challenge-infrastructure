package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Financial;

public interface FinancialService {
   Financial getOne(Integer id);
   List<Financial> getByContractAndType(Contract contract, String type);
   Financial save(Financial entity);
   List<Financial> save(List<Financial> entities);
   List<Financial> listAll();
}