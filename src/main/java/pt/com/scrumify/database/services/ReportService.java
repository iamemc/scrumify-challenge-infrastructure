package pt.com.scrumify.database.services;

import java.util.List;
import java.util.Map;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.entities.ReportView;
import pt.com.scrumify.entities.ResourceReport;

public interface ReportService {

   Map<Resource, List<ResourceReport>> reportByResource(List<Timesheet> timesheets);
   ResourceReport report(Timesheet timesheet);
   Map<Resource, List<ResourceReport>> monthlyReport(Timesheet timesheet, Map<Resource, List<ResourceReport>> resourceReport);
   Map<Contract, List<ResourceReport>> reportByWorkItem(ReportView reportView);
   Map<Team, List<ResourceReport>> reportByTeam(ReportView reportView);
   Map<Contract, List<ResourceReport>> monthlyReportByWorkitem(WorkItem workitem, Map<Contract, List<ResourceReport>> workitemReport);
   ResourceReport reportByWorkItem(WorkItemWorkLog worklog);
   List<WorkItem> getWorkItems(ReportView reportView);
   Map<Team, List<ResourceReport>> monthlyReportByTeam(WorkItem workitem, Map<Team, List<ResourceReport>> workitemReport);

}