package pt.com.scrumify.database.services;

import pt.com.scrumify.database.entities.Cookie;
import pt.com.scrumify.database.entities.Resource;

public interface CookieService {
   Cookie getOne(String name, Resource resource);
   Cookie save(Cookie cookie);
}