package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Application;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.repositories.ApplicationRepository;


@Service
public class ApplicationServiceImpl implements ApplicationService {
   
   @Autowired
   private ApplicationRepository repository;

   @Override
   public Application getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public Application save(Application application) {
      return repository.save(application);
   }

   @Override
   public List<Application> listAll() {
      return repository.findAll();
   }
   
   @Override
   public List<Application> listAllSortedByName() {
      return repository.findAllByOrderByNameAsc();
   }
   
   @Override
   public List<Application> listAllByResource(Resource resource) {
      return repository.findDistinctBySubAreaTeamsMembersPkResourceOrderByNameAsc(resource);
   }
}