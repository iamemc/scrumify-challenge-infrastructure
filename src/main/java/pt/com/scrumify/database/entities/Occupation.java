package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_OCCUPATIONS)
public class Occupation implements Serializable {
   private static final long serialVersionUID = 2934107363617999060L;
   
   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @Column(name = "name", length = 50, nullable = false)
   private String name;

   @Getter
   @Setter
   @Column(name = "sortorder", nullable = false)
   private Integer order;

   @Getter
   @Setter
   @Column(name = "active", nullable = false)
   private Boolean active;
}