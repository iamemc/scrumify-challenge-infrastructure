package pt.com.scrumify.database.entities;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Embeddable;
import javax.persistence.ManyToOne;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Embeddable
@NoArgsConstructor
public class TeamContractPK implements Serializable {
   private static final long serialVersionUID = 8272515569932508251L;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Team team;

   @Getter
   @Setter
   @ManyToOne(cascade = CascadeType.ALL)
   private Contract contract;

   public TeamContractPK(Team team, Contract contract) {
      super();
      
      this.team = team;
      this.contract = contract;
   }
}