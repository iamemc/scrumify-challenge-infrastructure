package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;

public interface ResourceRepository extends JpaRepository<Resource, Integer> {
   Resource getByEmail(String email);
   Resource getByUsername(String username);
   Resource getByUsernameAndActive(String username, boolean active);
   
   @Query(nativeQuery = false, 
         value = "SELECT r " + 
                 "FROM Resource r " +
                 "INNER JOIN r.profile p " + 
                 "WHERE p.system = FALSE " +
                 "ORDER BY p.id, r.name ")
   List<Resource> getNonSystem();
   
   List<Resource> findByProfileSystemFalseOrderByName();   
   
   List<Resource> getByApproved(boolean approved);
   
   @Query(nativeQuery = false, 
         value = "SELECT r " + 
                 "FROM Resource r " +
                 "INNER JOIN r.profile p " + 
                 "WHERE p.system = FALSE " +
                 "AND r.active = TRUE " +
                 "AND r.approved = TRUE " + 
                 "ORDER BY p.id, r.name ")
   List<Resource> getNonSystemUsersOrderByProfileAndName();
   
   @Query(nativeQuery = false, 
         value = "SELECT r " + 
                 "FROM Resource r " +  
                 "INNER JOIN r.profile p " + 
                 "WHERE r.active = TRUE " + 
                 "AND p.id IN (3) " + 
                 "ORDER BY r.name ")
  List<Resource> getResourcesByExecutiveProfile();
   
   @Query(nativeQuery = false,
         value = "SELECT r " +
                 "FROM Resource r " +
                 "INNER JOIN r.teamMembers tm " +
                 "WHERE tm.pk.team = :team " +
                 "ORDER BY r.name")
   List<Resource> getByTeam(@Param("team")Team team);
   
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT r " +
                 "FROM Resource r " +
                 "INNER JOIN r.teamMembers tm " +
                 "WHERE tm.pk.team IN (:list) " +
                 "ORDER BY r.name ASC")
   List<Resource> getByTeams(@Param("list") List<Team> team);
   
   @Query(nativeQuery = false,
         value = "SELECT DISTINCT r " +
                 "FROM Resource r " +
                 "INNER JOIN r.teamMembers tm " +
                 "WHERE tm.pk.team IN (:list) " +
                 "ORDER BY r.name ASC")
   List<Resource> findWithTimesheetsToReview(@Param("list") List<Team> team);
   
   @Query(nativeQuery = false,
         value = "SELECT r " +
                 "FROM Resource r " +
                 "JOIN FETCH r.timesheets sheet " +
                 "WHERE sheet.resource IN (:list) " +
                 "AND sheet.submitted = TRUE " +
                 "AND sheet.startingDay.year.id = :year " +
                 "AND sheet.startingDay.month = :month " +
                 "AND sheet.startingDay.day <= :day " +
                 "AND sheet.endingDay.day >= :day " +
                 "ORDER BY sheet.submitted ASC ")
   List<Resource> getResourcesOfTimesheetSubmit(@Param("list") List<Resource> resource, @Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);
   
   
   @Query(nativeQuery = false,
         value = "SELECT r " +
                 "FROM Resource r " +
                 "JOIN FETCH r.timesheets sheet " +
                 "WHERE sheet.resource IN (:list) " +
                 "AND sheet.submitted = TRUE " +
                 "AND sheet.reviewed = FALSE " +
                 "AND sheet.startingDay.year.id = :year " +
                 "AND sheet.startingDay.month = :month " +
                 "AND sheet.startingDay.day <= :day " +
                 "AND sheet.endingDay.day >= :day " +
                 "ORDER BY sheet.submitted ASC ")
   List<Resource> getTop3ResourcesWithTimesheetUnreviewed(@Param("list") List<Resource> resource, @Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);
   
   @Query(nativeQuery = false,
         value = "SELECT r " +
                 "FROM Resource r " +
                 "JOIN FETCH r.timesheets sheet " +
                 "WHERE sheet.resource IN (:list) " +
                 "AND sheet.submitted = TRUE " +
                 "AND sheet.approved = FALSE " +
                 "AND sheet.startingDay.year.id = :year " +
                 "AND sheet.startingDay.month = :month " +
                 "AND sheet.startingDay.day <= :day " +
                 "AND sheet.endingDay.day >= :day " +
                 "ORDER BY sheet.submitted ASC ")
   List<Resource> getTop3ResourcesWithTimesheetUnapproved(@Param("list") List<Resource> resource, @Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);

   @Query(nativeQuery = false,
           value = "SELECT r " +
                   "FROM Resource r " +
                   "JOIN FETCH r.timesheets sheet " +
                   "WHERE sheet.resource IN (:list) " +
                   "AND sheet.submitted = TRUE " +
                   "AND sheet.reviewed = FALSE " +
                   "AND sheet.approved = FALSE " +
                   "ORDER BY sheet.submitted ASC ")
     List<Resource> getResourcesWithTimesheetUnreviewed(@Param("list") List<Resource> resource);
     
     @Query(nativeQuery = false,
           value = "SELECT r " +
                   "FROM Resource r " +
                   "JOIN FETCH r.timesheets sheet " +
                   "WHERE sheet.resource IN (:list) " +
                   "AND sheet.submitted = TRUE " +
                   "AND sheet.approved = FALSE " +
                   "AND sheet.startingDay.year.id = :year " +
                   "AND sheet.startingDay.month = :month " +
                   "AND sheet.startingDay.day <= :day " +
                   "AND sheet.endingDay.day >= :day " +
                   "ORDER BY sheet.submitted ASC ")
     List<Resource> getResourcesWithTimesheetUnapproved(@Param("list") List<Resource> resource, @Param("year") Integer year, @Param("month") Integer month, @Param("day") Integer day);

     @Query(value = "SELECT DISTINCT m.pk.resource " + 
                    "FROM SubArea s " + 
                    "JOIN s.teams t " +  
                    "JOIN t.members m " +
                    "WHERE m.approver IS TRUE " +
                    "AND s = :subArea ")
     List<Resource> getApproversBySubArea(@Param("subArea") SubArea subArea);
     
     @Query(value = "SELECT DISTINCT r " + 
           "FROM SubArea s " + 
           "JOIN s.teams t " +  
           "JOIN t.members m " +
           "JOIN m.pk.resource r " +
           "JOIN r.profile p " + 
           "WHERE s = :subArea " + 
           "AND p.system IS FALSE " + 
           "AND r.active IS TRUE " + 
           "AND r.approved IS TRUE ")
     List<Resource> getBySubArea(@Param("subArea") SubArea subArea);
     
     List<Resource> findDistinctByTeamMembersPkTeamInAndTeamMembersPkTeamContracts(List<Team> teams, Contract contract);

     @Query(nativeQuery = false,
           value = "SELECT r " +
                   "FROM Resource r " +
                   "JOIN FETCH r.timesheets sheet " +
                   "INNER JOIN r.teamMembers members " +
                   "INNER JOIN members.pk.team.contracts teamcontract " +
                   "WHERE sheet.submitted = TRUE " +
                   "AND sheet.approved = TRUE " +
                   "AND sheet.startingDay.year.id = :year " +
                   "AND sheet.startingDay.month = :month " +
                   "AND sheet.startingDay.fortnight = :fortnight " +
                   "AND members.pk.team IN (:teams) " +
                   "AND teamcontract.pk.contract = :contract ")
     List<Resource> getResourcesWithTimesheetApproved(@Param("month") Integer month,@Param("year") Integer year, @Param("fortnight") Integer fortnight,@Param("teams") List<Team> teams,@Param("contract") Contract contract);
     
     
     @Query(nativeQuery = false,
           value = "SELECT DISTINCT r " +
                   "FROM Resource r " +
                   "INNER JOIN r.teamMembers tm " +
                   "INNER JOIN tm.pk.team.contracts teamcontract " +
                   "WHERE tm.pk.team IN (:list) " +
                   "AND teamcontract.pk.contract = :contract ")
     List<Resource> getByTeamsAndContract(@Param("list") List<Team> team,@Param("contract") Contract contract);
    
     @Query(value = "SELECT DISTINCT r " +
                    "FROM Resource r " +
                    "JOIN r.teamMembers tm " +
                    "JOIN tm.pk.team.contracts teamcontract " +
                    "WHERE teamcontract.pk.contract = :contract " +
                    "ORDER BY r.name ASC ")
     List<Resource> getMembersOfSameTeamAndContract(@Param("contract") Contract contract);
     
     @Query(nativeQuery = false,
           value = "SELECT r " +
                   "FROM Resource r " +
                   "JOIN r.teamMembers tm " +
                   "WHERE tm.pk.team = :team " +
                   "AND r <> :resource " +
                   "ORDER BY r.name")
     List<Resource> getByTeamExceptCurrentResource(@Param("team")Team team, @Param("resource") Resource resource);
     
}
