package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.TeamMemberPK;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Team;

public interface TeamMemberRepository extends JpaRepository<TeamMember, TeamMemberPK> {
   @Query(nativeQuery = false,
          value = "SELECT tm " +
                  "FROM TeamMember tm " +
                  "WHERE tm.pk.team = :team " +
                  "ORDER BY tm.pk.resource.name")
   List<TeamMember> getByTeam(@Param("team") Team team);
   
   @Query(nativeQuery = false,
         value = "SELECT case when (count(tm) > 0)  then true else false end " +
                 "FROM TeamMember tm " +
                 "WHERE tm.pk.team.id = :team " +
                 "AND tm.pk.resource = :resource")
   boolean isMemberOfTeam(@Param("resource") Resource resource, @Param("team") Integer team);

   List<TeamMember> findByPkResource(Resource resource);
   
   List<TeamMember> findByPkResourceAndApproverIsTrueOrReviewerIsTrue(Resource resource);
}