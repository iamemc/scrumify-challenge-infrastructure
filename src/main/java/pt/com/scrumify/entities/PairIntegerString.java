package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;

public class PairIntegerString {
   @Getter
   @Setter
   private int index;
   
   @Getter
   @Setter
   private String value;
   
   public PairIntegerString(int index, String value) {
      this.index = index;
      this.value = value;
   }
}