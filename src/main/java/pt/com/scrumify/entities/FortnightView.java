package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Timesheet;

public class FortnightView {
   
   @Getter
   @Setter
   private Timesheet timesheet;
   
   @Getter
   @Setter
   private String project;
   
   @Getter
   @Setter
   private Integer analysis;
   
   @Getter
   @Setter
   private Integer design;
   
   @Getter
   @Setter
   private Integer development;
   
   @Getter
   @Setter
   private Integer test;
   
   @Getter
   @Setter
   private Integer other;
   
   @Getter
   @Setter
   private Integer total;
   
   
}