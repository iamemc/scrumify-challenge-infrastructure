package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Contract;

public class ContractApprovalView {
   
   @Getter
   @Setter
   private Contract contract;
   
   @Getter
   @Setter
   private int hoursPreviousYear;
   
   @Getter
   @Setter
   private int hoursCurrentYear;
   
   @Getter
   @Setter
   private int january;
   
   @Getter
   @Setter
   private int february;
   
   @Getter
   @Setter
   private int march;
   
   @Getter
   @Setter
   private int april;
   
   @Getter
   @Setter
   private int may;
   
   @Getter
   @Setter
   private int june;
   
   @Getter
   @Setter
   private int july;
   
   @Getter
   @Setter
   private int august;
   
   @Getter
   @Setter
   private int september;
   
   @Getter
   @Setter
   private int october;
   
   @Getter
   @Setter
   private int november;
   
   @Getter
   @Setter
   private int december;
   
   @Setter
   private int total;
   
   @Setter
   private int balance;
   
   public int getTotal() {
      return this.january + this.february + this.march + this.april + this.may + this.june + this.july + this.august + this.september + this.october + this.november + this.december;
   }
   
   public int getBalance() {
      return this.contract.getHours() - getTotal();
   }

}
