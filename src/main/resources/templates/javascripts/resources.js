$(document).ready(function() {
   var id = $("input[type=hidden][name='id']").val();
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_CREATE}}]];
   
   if (id != 0) {
      url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_UPDATE} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   }
   
   ajaxtab(true, url, true, function(settings) {
      switch(settings.urlData.tab) {
         case "avatars":
            getavatars();
            break;
         case "teams":
             getteams();
             break;
         case "personalinfo":
         default:
            getpersonalinfo();
            break;
      }
   });
   
   initialize();
});
        
function initialize() {
   var json = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
   
   $('.ui.radio.checkbox').checkbox();
   $('.ui.toggle.checkbox').checkbox();
   $('.ui.dropdown').dropdown();
   $('select.dropdown').dropdown();
   $('.ui.avatar.image').popup({ position: 'top center' });
   
   calendar('startingDay', 'date', json);
   calendar('endingDay', 'date', json);

   formValidation();
   ongenderchange();
}

function getpersonalinfo() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_RESOURCES_AJAX}}]];
   
   ajaxpost(url, $("#resources-form").serialize(), "div[data-tab='personalinfo']", false, function() {
      initialize();
   }); 
};

function getavatars() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_AVATARS_SIGNUP_AJAX}}]];
   
   ajaxpost(url, $("#resources-form").serialize(), "div[data-tab='avatars']", false, function() {
      $('.ui.radio.checkbox').checkbox();
           
      formValidation();
   });
};
  
function getgender() {
   var gender = $("input[type=radio][name='gender']:checked").val();
     
   if (gender == null)
      return 1;
   else
      return gender;
};

function getteams() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PUBLIC_RESOURCES_TEAMS_AJAX} + ${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}}]] + id;
   
	ajaxget(url, "div[data-tab='teams']", function() {           
		initialize();   
	});
};

function ongenderchange() {
   $("input[type=radio][name='gender']").change(function() {
      $("input[type=radio][name='avatar']").prop('checked', false);
   });
}

function addresource(id,team) {
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TEAMMEMBERS}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + team + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id;
	
	ajaxget(url, "div[id='modal']", function() {
		$('.ui.dropdown').dropdown();
		$('.ui.toggle.checkbox').checkbox();
        $('#allocation-range').range({
            min: 0,
            max: 100,
            step: 1,
            start: $('#allocation').val(),
            input: '#allocation',
            onChange: function(value) {
               $('#allocation').val(value);
            }
         });
        $('#allocation').blur(function(){
	        if($('#allocation').val()>=0 && $('#allocation').val()<=100 ){
		       	 $('#allocation-range').range({
		                min: 0,
		                max: 100,
		                step: 1,
		                input: '#allocation',
		       		 start: $('#allocation').val()
		       	 });  
	        }
	    }).blur();
        $('form').form({
            on: 'blur',
            inline : true,
            fields: { 
               occupation : {
                  identifier : 'occupation',
                  rules : [{
                     type : 'empty',
                     prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
                  }]
               }
            },
            onSuccess : function(event) {
               event.preventDefault();
               
               var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCE_TEAMS_SAVE}}]];
               ajaxpost(url, $("#teammembers-form").serialize(), "div[id='teams-list']", true, function() {
            	   initialize();
               });
            }
       });
   }, true);
};

function removeresource(id,team) {
    var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCE_TEAMS_DELETE}}]] + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + id + [[${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SLASH}]] + team;  
    
    ajaxdelete(url, "div[id='teams-list']", function() {
    	initialize();
     },false);     
};

function formSubmission() {   
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_SAVE}}]];
   
   ajaxpost(url, $("#resources-form").serialize(), "div[data-tab='personalinfo']", false, function() {
      initialize();
   });
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
         abbreviation : {
            identifier : 'abbreviation',
            rules : [ {
               type : 'exactLength[3]',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_LENGTH}(3)}]]
            } ]
         },
         name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         email : {
            identifier : 'email',
            rules : [ {
               type : 'email',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMAIL}}]]
            } ]
         },
         startingDay : {
            identifier : 'startingDay',
            rules : [ {
               type : 'empty',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         profile : {
            identifier : 'profile',
            rules : [ {
               type : 'empty',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         username : {
            identifier : 'username',
            rules : [ {
               type : 'empty',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         code : {
            identifier : 'code',
            rules : [ {
               type : 'empty',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         codeRegEx : {
            identifier : 'code',
            rules : [ {
               type : 'regExp',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EXTERNAL_CODE}}]],
               value : '[0-9]{3}-[a-z]{2,3}'
            } ]
         },
         gender : {
            identifier : 'gender',
            rules : [ {
               type : 'checked',
               prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_CHOICE}(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_COMMON_GENDER}})}]]
            } ]
         },
         avatar : {
            identifier : 'avatar',
            rules : [ {
               type : 'checked',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY_CHOICE}(#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_COMMON_AVATAR}})}]]
            } ]
         }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}