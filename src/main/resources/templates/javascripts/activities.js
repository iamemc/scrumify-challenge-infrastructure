$(document).ready(function() {
  $('select.dropdown').dropdown();
    
  formValidation();
});

function formValidation() {
  $('form').form({
    on : 'blur',
    inline : true,
    fields : {
      number : {
        identifier : 'number',
        rules : [ {
          type : 'empty',
          prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      },
      name : {
        identifier : 'name',
        rules : [ {
          type : 'empty',
          prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      },
      projectphase : {
        identifier : 'projectPhase',
        rules : [ {
          type : 'empty',
          prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
        } ]
      }
    },           	  
    onSuccess : function(event) {
      event.preventDefault();
         
      var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_ACTIVITIES_SAVE}}]];
      ajaxpost(url, $("#activities-form").serialize(), "body", false);
    }
  });
}