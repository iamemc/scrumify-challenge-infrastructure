CREATE VIEW v_timesheetsapproval AS
 SELECT tsb.id,
    tsb.resource,
    tsb.year,
    tsb.month,
    tsb.fortnight,
    tsb.code,
    1 AS tow,
    tsb.analysis AS hours
   FROM timesheetsapproval tsb
  WHERE (tsb.analysis > 0)
UNION ALL
 SELECT tsb.id,
    tsb.resource,
    tsb.year,
    tsb.month,
    tsb.fortnight,
    tsb.code,
    2 AS tow,
    tsb.design AS hours
   FROM timesheetsapproval tsb
  WHERE (tsb.design > 0)
UNION ALL
 SELECT tsb.id,
    tsb.resource,
    tsb.year,
    tsb.month,
    tsb.fortnight,
    tsb.code,
    3 AS tow,
    tsb.development AS hours
   FROM timesheetsapproval tsb
  WHERE (tsb.development > 0)
UNION ALL
 SELECT tsb.id,
    tsb.resource,
    tsb.year,
    tsb.month,
    tsb.fortnight,
    tsb.code,
    4 AS tow,
    tsb.test AS hours
   FROM timesheetsapproval tsb
  WHERE (tsb.test > 0)
UNION ALL
 SELECT tsb.id,
    tsb.resource,
    tsb.year,
    tsb.month,
    tsb.fortnight,
    tsb.code,
    5 AS tow,
    tsb.other AS hours
   FROM timesheetsapproval tsb
  WHERE (tsb.other > 0);